package ca.polymtl.log4420
package snippet

import net.liftweb._
import http.js.JsCmds
import http.SHtml
import util.CssSel
import util.Helpers._

import xml.NodeSeq

object Cours
{
  def apply( cours: model.Cours, cheminement: model.Cheminement ): NodeSeq => NodeSeq =
  {
  	".cours [id]" #> cours.sigle &
    ".sigle *" #> cours.sigle &
    ".titre *" #> cours.titre &
    ".credits *" #> cours.credit.toString &
    ".disponibilite" #> ( "li *" #> cours.disponibilite.map( _.toString ) ) &
    ".pre-requis" #> ( "li *" #> cours.prerequis.map( _.sigle ) ) &
    ".co-requis" #> ( "li *" #> cours.corequis.map( _.sigle ) ) &
    ".triplet .theorie *" #> cours.triplet.theorie.toString &
    ".triplet .lab *" #> cours.triplet.lab.toString &
    ".ua .genie *" #> cours.ua.genie.toString &
    ".ua .conception *" #> cours.ua.conception.toString &
    ".ua .math *" #> cours.ua.math.toString &
    ".ua .science *" #> cours.ua.science.toString &
    ".ua .complementaire *" #> cours.ua.complementaire.toString &
    ".modifier [href]" #> ModifierCours.cheminement.calcHref( cheminement, cours )
  }
}