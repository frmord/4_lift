!!! __Faites un fork__ !!!

# Objectifs du travail pratique

* Transformer un modèle en html à l'aide de snippet.
* Comprendre l'interaction client-serveur
* Implémenter des tests unitaires
* Implémenter des tests Seleniums

# Présentation des fonctionnalités à implémenter

* [Disponible sur Trello](https://trello.com/board/cheminement-poly-tp4/50816c3f3d1baaad1e0055ef)
* Faites vous une copie ( Menu à gauche du titre ­­­­­| Copy Board )

# Description des critères d'évaluation

* Équipe de deux
  * Dans la colone Équipe de deux, le premier pourcentage représente votre poctuation pour cette tâche

* Équipe de quatre
  * Dans la colone Équipe de deux, le deuxième pourcentage représente votre poctuation pour cette tâche
  * Dans la colone Équipe de quatre, le pourcentage représente votre poctuation pour cette tâche
	
# Présentation sommaire des technologies et outils utilisés

Avant de commencer le tp assurez-vous de bien comprendre les concepts de lift suivants

* [CSS Transformation](http://doc.scalakata.com/snippet/transformation/lift)
* [Javascript binding](http://cookbook.liftweb.net/Trigger+server-side+code+from+a+button+press.html)
* [Snippet & View First](http://simply.liftweb.net/index-Chapter-3.html#toc-Chapter-3)
* [Loc](http://simply.liftweb.net/index-3.2.html#toc-Subsection-3.2.7)
* [Ajax Bidirectionnel](http://cookbook.liftweb.net/Show+a+template+inside+a+page+dynamically.html)

# Instructions concernant la remise

* via moodle dans un zip
* faites ```sbt clean``` avant de faire votre zip 
* -10% s'il y a des fichiers/dossier non pertinants ( voir .gitignore )
* le nom et le matricule de chaque membre de l'équipe dans /src/main/scala/ca/polymtl/log4420/model/EquipeProjet.scala

# Références

* [Simply Lift](http://simply.liftweb.net/)
* [Cookbook](http://cookbook.liftweb.net/)
* [Wiki](https://www.assembla.com/spaces/liftweb/wiki)
* [Mon effort de documentation](http://doc.scalakata.com)

# Glossaire

* Programme: Un ensemble de cheminement regroupé sous un génie ( ex: programme de génie logiciel )
* Cheminement: Un ensemble partiellement ordoné de cours organisé chronologiquement
* Comité de programme: Groupe de personne qui modifie les cheminements ( ajoute/suprime/modifie des cours )
* Prérequis: Règle de cours à faire avant ( ex: A ou B sont prérequis à C: je dois avoir finnit A ou B pour faire C )
* Corequis: identique à Prérequis, sauf qu'il est possible de faire les cours en même temps
* Disponibilité: le plan triénal ( trois ans ) définit les semestres où le cours est enseigné
* Session: les cours que l'on enseigne pendant une certaine période ( déformation http://www.cnrtl.fr/definition/session )
* Periode: le moment de l'année où on enseigne ( Hiver, Été, Automne ) 